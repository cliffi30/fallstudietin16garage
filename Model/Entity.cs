namespace Model
{
    using System;
    using System.Data.Entity;

    public class Entity : DbContext
    {
        // Your context has been configured to use a 'Entity' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'Model.Entity' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'Entity' 
        // connection string in the application configuration file.
        public Entity()
            : base("name=Entity")
        {
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        public virtual DbSet<Car> Cars { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderItem> OrderItems { get; set; }
        public virtual DbSet<Product> Products { get; set; }
        public virtual DbSet<Sale> Sales { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<User> Users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Setup two foreign keys from the same table (client) in order.
            modelBuilder.Entity<Order>()
                .HasRequired(o => o.Seller)
                .WithMany(c => c.Sellers)
                .HasForeignKey(o => o.SellerId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasRequired(o => o.Buyer)
                .WithMany(c => c.Buyers)
                .HasForeignKey(o => o.BuyerId)
                .WillCascadeOnDelete(false);

            base.OnModelCreating(modelBuilder);
        }

        static Entity()
        {
            var type = typeof(System.Data.Entity.SqlServer.SqlProviderServices);
            if (type == null)
                throw new Exception("Do not remove, ensures static reference to System.Data.Entity.SqlServer");
        }

    }
}