namespace Model.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Inital : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cars",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Brand = c.String(),
                        Type = c.String(),
                        EngineDisplacement = c.Int(nullable: false),
                        ConstructionYear = c.Int(nullable: false),
                        VehicleIdentificationNumber = c.String(),
                        LicensePlateNumber = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SellerId = c.Int(nullable: false),
                        BuyerId = c.Int(nullable: false),
                        CarId = c.Int(),
                        UserId = c.Int(nullable: false),
                        Date = c.DateTime(nullable: false),
                        OrderStatus = c.Int(nullable: false),
                        InvoiceStatus = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.BuyerId)
                .ForeignKey("dbo.Cars", t => t.CarId)
                .ForeignKey("dbo.Clients", t => t.SellerId)
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .Index(t => t.SellerId)
                .Index(t => t.BuyerId)
                .Index(t => t.CarId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        ClientId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                        PhoneNumber = c.String(),
                    })
                .PrimaryKey(t => t.ClientId);
            
            CreateTable(
                "dbo.OrderItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderId = c.Int(nullable: false),
                        Discount = c.Int(nullable: false),
                        Amount = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.OrderId, cascadeDelete: true)
                .Index(t => t.OrderId);
            
            CreateTable(
                "dbo.Products",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        OrderItemId = c.Int(nullable: false),
                        Description = c.String(),
                        Price = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.OrderItems", t => t.OrderItemId, cascadeDelete: true)
                .Index(t => t.OrderItemId);
            
            CreateTable(
                "dbo.Sales",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        WorkingRapport = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Services",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        CreditCheckPassed = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Orders", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Orders", "UserId", "dbo.Users");
            DropForeignKey("dbo.Services", "Id", "dbo.Orders");
            DropForeignKey("dbo.Orders", "SellerId", "dbo.Clients");
            DropForeignKey("dbo.Sales", "Id", "dbo.Orders");
            DropForeignKey("dbo.Products", "OrderItemId", "dbo.OrderItems");
            DropForeignKey("dbo.OrderItems", "OrderId", "dbo.Orders");
            DropForeignKey("dbo.Orders", "CarId", "dbo.Cars");
            DropForeignKey("dbo.Orders", "BuyerId", "dbo.Clients");
            DropIndex("dbo.Services", new[] { "Id" });
            DropIndex("dbo.Sales", new[] { "Id" });
            DropIndex("dbo.Products", new[] { "OrderItemId" });
            DropIndex("dbo.OrderItems", new[] { "OrderId" });
            DropIndex("dbo.Orders", new[] { "UserId" });
            DropIndex("dbo.Orders", new[] { "CarId" });
            DropIndex("dbo.Orders", new[] { "BuyerId" });
            DropIndex("dbo.Orders", new[] { "SellerId" });
            DropTable("dbo.Users");
            DropTable("dbo.Services");
            DropTable("dbo.Sales");
            DropTable("dbo.Products");
            DropTable("dbo.OrderItems");
            DropTable("dbo.Clients");
            DropTable("dbo.Orders");
            DropTable("dbo.Cars");
        }
    }
}
