﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class OrderItem
    {
        [Key]
        public int Id { get; set; }
        public int OrderId { get; set; }

        public int Discount { get; set; }
        public int Amount { get; set; }

        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }
        public virtual ICollection<Product> Products { get; set; }
    }
}
