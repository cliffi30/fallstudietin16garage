USE [master]
GO
/****** Object:  Database [TIN16Garage]    Script Date: 09/3/2019 4:49:25 PM ******/
CREATE DATABASE [TIN16Garage];
GO

USE [master]
GO
/****** Object:  Login [TIN16Garage]    Script Date: 09/3/2019 11:49:26 AM ******/
IF NOT EXISTS (SELECT name FROM [master].[dbo].syslogins WHERE name = N'TIN16Garage')
CREATE LOGIN [TIN16GarageAdministrator] WITH PASSWORD=N'TIN16GarageAdministrator_01', DEFAULT_DATABASE=[TIN16Garage], DEFAULT_LANGUAGE=[Deutsch], CHECK_EXPIRATION=OFF, CHECK_POLICY=ON
GO

ALTER AUTHORIZATION ON DATABASE::[TIN16Garage] TO [sa]
GO
USE [TIN16Garage]
GO
/****** Object:  User [MarketAdministrator]    Script Date: 09/3/2019 4:49:25 PM ******/
CREATE USER [TIN16GarageAdministrator] FOR LOGIN [TIN16GarageAdministrator] WITH DEFAULT_SCHEMA=[dbo]
GO
sys.sp_addrolemember @rolename = N'db_owner', @membername = N'TIN16GarageAdministrator'
GO
GRANT CONNECT TO [TIN16GarageAdministrator] AS [dbo]
GO

USE [master]
GO
ALTER LOGIN TIN16GarageAdministrator WITH DEFAULT_DATABASE = TIN16Garage
GO
