﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class Service
    {
        [ForeignKey("Order")]
        public int Id { get; set; }

        public bool CreditCheckPassed { get; set; }

        public virtual Order Order {get; set; }
    }
}
