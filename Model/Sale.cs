﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class Sale
    {
        [ForeignKey("Order")]
        public int Id { get; set; }

        public string WorkingRapport { get; set; }

        public virtual Order Order { get; set; }
    }
}
