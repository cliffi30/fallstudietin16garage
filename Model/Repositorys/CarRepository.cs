﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Repositorys
{
    public class CarRepository
    {
        public void Insert(Car car)
        {
            try
            {
                using (Entity ctx = new Entity())
                {
                    ctx.Cars.Add(car);
                    ctx.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to insert new car", ex);
            }
        }

        public List<Car> GetCars()
        {
            try
            {
                List<Car> list = new List<Car>();
                using (Entity ctx = new Entity())
                {
                    list = ctx.Cars.ToList();
                    return list;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to load cars from database", ex);
            }
        }
    }
}
