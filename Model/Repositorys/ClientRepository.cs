﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Model.Repositorys
{
    public class ClientRepository
    {
        public List<Client> GetAll()
        {
            try
            {
                List<Client> list = new List<Client>();
                using (Entity ctx = new Entity())
                {
                    list = ctx.Clients.ToList();
                    return list;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to get all clients", ex);
            }
        }


        public void Insert(Client client)
        {
            try
            {
                using (Entity ctx = new Entity())
                {
                    ctx.Clients.Add(client);
                    ctx.SaveChanges();
                }
            }
            catch(Exception ex)
            {
                throw new Exception("Failed to insert new client", ex);
            }
        }
    }
}
