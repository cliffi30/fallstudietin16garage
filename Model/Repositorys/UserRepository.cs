﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Repositorys
{
    public class UserRepository
    {
        public List<User> GetUsers()
        {
            try
            {
                List<User> list = new List<User>();
                using (Entity ctx = new Entity())
                {
                    list = ctx.Users.ToList();
                    return list;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to load users from database", ex);
            }
        }
    }
}

