﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model.Repositorys
{
    public class OrderRepository
    {
        public Order GetOrderByID(int id)
        {
            var ret = new Order();
            try
            {
                using (Entity ctx = new Entity())
                {
                   ret = ctx.Orders.FirstOrDefault(x => x.Id == id);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return ret;
        }
    }
}
