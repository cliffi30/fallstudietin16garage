﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class User
    {
        [Key]
        public int Id { get; set; }

        public string Name { get; set; }
        public virtual ICollection<Order> Orders { get; set; }

        public User()
        {

        }

        public User(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
