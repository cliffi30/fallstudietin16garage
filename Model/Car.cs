﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class Car
    {
        [Key]
        public int Id { get; set; }
        
        public string Name { get; set; }
        public string Brand { get; set; }
        public string Type { get; set; }    // Why?
        public int EngineDisplacement { get; set; }
        public int ConstructionYear { get; set; }
        public string VehicleIdentificationNumber { get; set; }
        public string LicensePlateNumber { get; set; }



        public virtual ICollection<Order> Orders { get; set; }

        public string Display {
            get
            {
                return Name + " " + VehicleIdentificationNumber;
            }
        }

        public Car()
        {

        }

        public Car(string name, string brand, string type,
            int engine, int construction, string identification, string license)
        {
            Name = name;
            Brand = brand;
            Type = type;
            EngineDisplacement = engine;
            ConstructionYear = construction;
            VehicleIdentificationNumber = identification;
            LicensePlateNumber = license;
        }

        public Car(int id, string name)
        {
            Id = id;
            Name = name;
        }

        //git test
    }
}
