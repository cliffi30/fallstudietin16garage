﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public class Product
    {
        [Key]
        public int Id { get; set; }
        public int OrderItemId { get; set; }

        public string Description { get; set; }
        public int Price { get; set; }
        
        [ForeignKey("OrderItemId")]
        public virtual OrderItem OrderItem { get; set; }
    }
}
