﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class Client
    {
        [Key]
        public int ClientId { get; set; }

        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }

        public virtual ICollection<Order> Sellers { get; set; }
        public virtual ICollection<Order> Buyers { get; set; }

        public Client()
        {

        }

        public Client(string name, string address, string phoneNumber)
        {
            Name = name;
            Address = address;
            PhoneNumber = phoneNumber;
        }

        public Client(int id, string name, string address, string phoneNumber) : this(name, address, phoneNumber)
        {
            ClientId = id;
            Name = name;
            Address = address;
            PhoneNumber = phoneNumber;
        }

        public Client(int id, string name)
        {
            ClientId = id;
            Name = name;
        }
    }
}
