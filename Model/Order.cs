﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    public enum OrderStatus
    {
        OFFERED,
        ORDERED
    }

    public enum InoviceStatus
    {
        OPENED,
        CLOSED,
        INVOICED
    }

    public class Order
    {
        [Key]
        public int Id { get; set; }
        public int SellerId { get; set; }       // Is to be implemented in "OnModelCreateing in Entity.cs"
        public int BuyerId { get; set; }        // Is to be implemented in "OnModelCreateing in Entity.cs"
        public int? CarId { get; set; }
        public int UserId { get; set; }

        public DateTime Date { get; set; }
        public OrderStatus OrderStatus { get; set; }
        public InoviceStatus InvoiceStatus { get; set; }

        public virtual Sale Sale { get; set; }
        public virtual Service Service { get; set; }
        public virtual ICollection<OrderItem> OrderItems { get; set; }
        public virtual Client Seller { get; set; }  // Is to be implemented in "OnModelCreateing in Entity.cs"
        public virtual Client Buyer { get; set; }   // Is to be implemented in "OnModelCreateing in Entity.cs"
        [ForeignKey("CarId")]
        public virtual Car Car { get; set; }
        [ForeignKey("UserId")]
        public virtual User User { get; set; }
    }
}
