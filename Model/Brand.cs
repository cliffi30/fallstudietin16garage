﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class Brand
    {
        [Key]
        public int Id { get; set; }
        
        public string Name { get; set; }
        public string Type { get; set; }


        public virtual ICollection<Order> Orders { get; set; }

    }
}
