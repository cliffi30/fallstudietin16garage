﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain
{
    public class Car
    {
        public int Weight { get; private set; }

        public Car(int weight)
        {
            Weight = weight;
        }

    }

}
