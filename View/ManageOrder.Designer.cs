﻿namespace View
{
    partial class ManageOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmbUser = new System.Windows.Forms.ComboBox();
            this.lblUser = new System.Windows.Forms.Label();
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnBack = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.cmbBuyer = new System.Windows.Forms.ComboBox();
            this.lblBuyer = new System.Windows.Forms.Label();
            this.cmbSeller = new System.Windows.Forms.ComboBox();
            this.lblSeller = new System.Windows.Forms.Label();
            this.cmbOrderStatus = new System.Windows.Forms.ComboBox();
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnNewClient = new System.Windows.Forms.Button();
            this.lblDatum = new System.Windows.Forms.Label();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.cmbCar = new System.Windows.Forms.ComboBox();
            this.lblCar = new System.Windows.Forms.Label();
            this.btnManageCar = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cmbUser
            // 
            this.cmbUser.FormattingEnabled = true;
            this.cmbUser.Location = new System.Drawing.Point(179, 342);
            this.cmbUser.Margin = new System.Windows.Forms.Padding(6);
            this.cmbUser.Name = "cmbUser";
            this.cmbUser.Size = new System.Drawing.Size(296, 33);
            this.cmbUser.TabIndex = 22;
            // 
            // lblUser
            // 
            this.lblUser.AutoSize = true;
            this.lblUser.Location = new System.Drawing.Point(13, 348);
            this.lblUser.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblUser.Name = "lblUser";
            this.lblUser.Size = new System.Drawing.Size(127, 25);
            this.lblUser.TabIndex = 21;
            this.lblUser.Text = "Erstellt von*";
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(23, 413);
            this.lblMessage.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(112, 25);
            this.lblMessage.TabIndex = 25;
            this.lblMessage.Text = "ErrorLabel";
            this.lblMessage.Visible = false;
            // 
            // btnBack
            // 
            this.btnBack.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnBack.Location = new System.Drawing.Point(19, 453);
            this.btnBack.Margin = new System.Windows.Forms.Padding(6);
            this.btnBack.Name = "btnBack";
            this.btnBack.Size = new System.Drawing.Size(150, 44);
            this.btnBack.TabIndex = 24;
            this.btnBack.Text = "Back";
            this.btnBack.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(611, 453);
            this.btnSave.Margin = new System.Windows.Forms.Padding(6);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(150, 44);
            this.btnSave.TabIndex = 23;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cmbBuyer
            // 
            this.cmbBuyer.FormattingEnabled = true;
            this.cmbBuyer.Location = new System.Drawing.Point(179, 127);
            this.cmbBuyer.Margin = new System.Windows.Forms.Padding(6);
            this.cmbBuyer.Name = "cmbBuyer";
            this.cmbBuyer.Size = new System.Drawing.Size(296, 33);
            this.cmbBuyer.TabIndex = 27;
            // 
            // lblBuyer
            // 
            this.lblBuyer.AutoSize = true;
            this.lblBuyer.Location = new System.Drawing.Point(13, 133);
            this.lblBuyer.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblBuyer.Name = "lblBuyer";
            this.lblBuyer.Size = new System.Drawing.Size(83, 25);
            this.lblBuyer.TabIndex = 26;
            this.lblBuyer.Text = "Käufer*";
            // 
            // cmbSeller
            // 
            this.cmbSeller.FormattingEnabled = true;
            this.cmbSeller.Location = new System.Drawing.Point(179, 73);
            this.cmbSeller.Margin = new System.Windows.Forms.Padding(6);
            this.cmbSeller.Name = "cmbSeller";
            this.cmbSeller.Size = new System.Drawing.Size(296, 33);
            this.cmbSeller.TabIndex = 29;
            // 
            // lblSeller
            // 
            this.lblSeller.AutoSize = true;
            this.lblSeller.Location = new System.Drawing.Point(13, 79);
            this.lblSeller.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblSeller.Name = "lblSeller";
            this.lblSeller.Size = new System.Drawing.Size(113, 25);
            this.lblSeller.TabIndex = 28;
            this.lblSeller.Text = "Verkäufer*";
            // 
            // cmbOrderStatus
            // 
            this.cmbOrderStatus.FormattingEnabled = true;
            this.cmbOrderStatus.Location = new System.Drawing.Point(179, 297);
            this.cmbOrderStatus.Margin = new System.Windows.Forms.Padding(6);
            this.cmbOrderStatus.Name = "cmbOrderStatus";
            this.cmbOrderStatus.Size = new System.Drawing.Size(296, 33);
            this.cmbOrderStatus.TabIndex = 31;
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(13, 303);
            this.lblStatus.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(81, 25);
            this.lblStatus.TabIndex = 30;
            this.lblStatus.Text = "Status*";
            // 
            // btnNewClient
            // 
            this.btnNewClient.Location = new System.Drawing.Point(494, 91);
            this.btnNewClient.Name = "btnNewClient";
            this.btnNewClient.Size = new System.Drawing.Size(247, 53);
            this.btnNewClient.TabIndex = 32;
            this.btnNewClient.Text = "Neuer Kunde anlegen";
            this.btnNewClient.UseVisualStyleBackColor = true;
            this.btnNewClient.Click += new System.EventHandler(this.btnNewClient_Click);
            // 
            // lblDatum
            // 
            this.lblDatum.AutoSize = true;
            this.lblDatum.Location = new System.Drawing.Point(13, 26);
            this.lblDatum.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblDatum.Name = "lblDatum";
            this.lblDatum.Size = new System.Drawing.Size(82, 25);
            this.lblDatum.TabIndex = 35;
            this.lblDatum.Text = "Datum*";
            // 
            // dtpDate
            // 
            this.dtpDate.Location = new System.Drawing.Point(179, 20);
            this.dtpDate.Margin = new System.Windows.Forms.Padding(6);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(296, 31);
            this.dtpDate.TabIndex = 34;
            // 
            // cmbCar
            // 
            this.cmbCar.FormattingEnabled = true;
            this.cmbCar.Location = new System.Drawing.Point(179, 194);
            this.cmbCar.Margin = new System.Windows.Forms.Padding(6);
            this.cmbCar.Name = "cmbCar";
            this.cmbCar.Size = new System.Drawing.Size(296, 33);
            this.cmbCar.TabIndex = 37;
            // 
            // lblCar
            // 
            this.lblCar.AutoSize = true;
            this.lblCar.Location = new System.Drawing.Point(13, 200);
            this.lblCar.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.lblCar.Name = "lblCar";
            this.lblCar.Size = new System.Drawing.Size(56, 25);
            this.lblCar.TabIndex = 36;
            this.lblCar.Text = "Auto";
            // 
            // btnManageCar
            // 
            this.btnManageCar.Location = new System.Drawing.Point(494, 184);
            this.btnManageCar.Name = "btnManageCar";
            this.btnManageCar.Size = new System.Drawing.Size(247, 53);
            this.btnManageCar.TabIndex = 38;
            this.btnManageCar.Text = "Neues Auto erfassen";
            this.btnManageCar.UseVisualStyleBackColor = true;
            this.btnManageCar.Click += new System.EventHandler(this.btnManageCar_Click);
            // 
            // ManageOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1030, 747);
            this.Controls.Add(this.btnManageCar);
            this.Controls.Add(this.cmbCar);
            this.Controls.Add(this.lblCar);
            this.Controls.Add(this.lblDatum);
            this.Controls.Add(this.dtpDate);
            this.Controls.Add(this.btnNewClient);
            this.Controls.Add(this.cmbOrderStatus);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.cmbSeller);
            this.Controls.Add(this.lblSeller);
            this.Controls.Add(this.cmbBuyer);
            this.Controls.Add(this.lblBuyer);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.btnBack);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.cmbUser);
            this.Controls.Add(this.lblUser);
            this.Name = "ManageOrder";
            this.Text = "CreateOrdercs";
            this.Load += new System.EventHandler(this.CreateOrder_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbUser;
        private System.Windows.Forms.Label lblUser;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Button btnBack;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.ComboBox cmbBuyer;
        private System.Windows.Forms.Label lblBuyer;
        private System.Windows.Forms.ComboBox cmbSeller;
        private System.Windows.Forms.Label lblSeller;
        private System.Windows.Forms.ComboBox cmbOrderStatus;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button btnNewClient;
        private System.Windows.Forms.Label lblDatum;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.ComboBox cmbCar;
        private System.Windows.Forms.Label lblCar;
        private System.Windows.Forms.Button btnManageCar;
    }
}