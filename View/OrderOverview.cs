﻿using System;
using System.Windows.Forms;
using View.Common;

namespace View
{
    public partial class OrderOverview : Form
    {
        public OrderOverview()
        {
            InitializeComponent();
            DataGridViewSetter.Set(dgvOrders);
        }

        /// <summary>
        /// Load initial UI data
        /// </summary>
        private void LoadInitalData()
        {
            //throw new NotImplementedException();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ManageOrder form = new ManageOrder();
            form.ShowDialog(this);
            LoadInitalData();
        }
    }
}
