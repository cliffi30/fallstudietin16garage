﻿using Controller;
using System;
using System.Windows.Forms;
using View.Common;

namespace View
{
    public partial class ManageCar : Form
    {
        public ManageCar()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(txtBrand.Text)
                && !String.IsNullOrEmpty(txtConstruction.Text)
                && !String.IsNullOrEmpty(txtEngine.Text)
                && !String.IsNullOrEmpty(txtIdentification.Text)
                && !String.IsNullOrEmpty(txtLicense.Text)
                && !String.IsNullOrEmpty(txtName.Text)
                && !String.IsNullOrEmpty(txtType.Text))
                {
                    CarService.CreateNewCar(txtName.Text, txtBrand.Text, txtType.Text,
                        txtEngine.Text, txtConstruction.Text, txtIdentification.Text, txtLicense.Text);
                    this.Close();
                }
                else
                {
                    MessageLabelSetter.SetMessageLabel(lblMessage, MessageLabelSetter.State.Info, "Bitte alle Felder füllen");
                }
            }
            catch(FormatException fex)
            {
                MessageLabelSetter.SetMessageLabel(lblMessage, MessageLabelSetter.State.Info, "Fehler beim validieren der Daten");
            }
            catch(Exception ex)
            {
                MessageLabelSetter.SetMessageLabel(lblMessage, MessageLabelSetter.State.Error, "Fehler beim speichern der Daten");
            }
        }
    }
}
