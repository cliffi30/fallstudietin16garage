﻿using Controller;
using System;
using System.Windows.Forms;
using View.Common;

namespace View
{
    public partial class AddClient : Form
    {
        public AddClient()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(!String.IsNullOrEmpty(txtClientName.Text) 
                && !String.IsNullOrEmpty(txtClientAddress.Text) 
                && !String.IsNullOrEmpty(txtClientPhone.Text))
            {
                ClientService.IntertNewUser(txtClientName.Text, txtClientAddress.Text, txtClientPhone.Text);
                this.Close();
            }
            else
            {
                MessageLabelSetter.SetMessageLabel(lblMessage, MessageLabelSetter.State.Info, "Bitte alle Felder füllen");
            }
            
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
