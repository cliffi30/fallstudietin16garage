﻿using System.Windows.Forms;

namespace View.Common
{
    /// <summary>
    /// Windows.Forms.DateTimePicker Setter by Dominic R.
    /// </summary>
    public static class DateTimePickerSetter
    {
        /// <summary>
        /// Sets default values for DateTimePicker
        /// </summary>
        /// <param name="dtp"></param>
        public static void Set(DateTimePicker dtp)
        {
            dtp.ShowUpDown = true;
            dtp.CustomFormat = "dd.MM.yyyy";
            dtp.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
        }
    }
}
