﻿using System.Windows.Forms;

namespace View.Common
{
    /// <summary>
    /// Windows.Forms.ComboBox Setter by Dominic R.
    /// </summary>
    public static class ComboBoxSetter
    {
        /// <summary>
        /// Sets default values for ComboBox
        /// </summary>
        /// <param name="cmb"></param>
        public static void Set(ComboBox cmb)
        {
            cmb.DropDownStyle = ComboBoxStyle.DropDownList;
        }
    }
}
