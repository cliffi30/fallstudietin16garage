﻿using System.Windows.Forms;

namespace View.Common
{
    /// <summary>
    /// Windows.Forms.DataGridView Setter by Dominic R.
    /// </summary>
    public static class DataGridViewSetter
    {
        /// <summary>
        /// Sets default values for DataGridView
        /// </summary>
        /// <param name="dgv"></param>
        public static void Set(DataGridView dgv)
        {
            dgv.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dgv.AllowUserToResizeColumns = false;
            dgv.RowHeadersVisible = false;
            dgv.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.Fill);
        }

    }
}
