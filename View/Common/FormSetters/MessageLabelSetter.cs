﻿using System;
using System.Drawing;

namespace View.Common
{
    /// <summary>
    /// Windows.Forms.Label Setter by Dominic R.
    /// </summary>
    public static class MessageLabelSetter
    {
        /// <summary>
        /// State enum for MessageLabel
        /// </summary>
        public enum State
        {
            Ok = 1,
            Error,
            Clear,
            Info
        }

        /// <summary>
        /// Sets the view label based on state enum
        /// </summary>
        /// <param name="lblMessage"></param>
        /// <param name="state"></param>
        public static void SetMessageLabel(System.Windows.Forms.Label lblMessage, State state)
        {
            switch(state)
            {
                case State.Ok:
                    SetMessageLabel(lblMessage, state, "Saved!");
                    break;
                case State.Error:
                    SetMessageLabel(lblMessage, state, "Error!");
                    break;
                case State.Clear:
                    lblMessage.Text = String.Empty;
                    lblMessage.Visible = false;
                    break;

            }
        }

        /// <summary>
        /// Sets the view label based on state enum and custom message
        /// </summary>
        /// <param name="lblMessage"></param>
        /// <param name="state"></param>
        /// <param name="message"></param>
        public static void SetMessageLabel(System.Windows.Forms.Label lblMessage, State state, string message)
        {
            switch (state)
            {
                case State.Ok:
                    lblMessage.Text = message;
                    lblMessage.ForeColor = Color.Green;
                    lblMessage.Visible = true;
                    break;
                case State.Error:
                    lblMessage.Text = message;
                    lblMessage.ForeColor = Color.Red;
                    lblMessage.Visible = true;
                    break;
                case State.Info:
                    lblMessage.Text = message;
                    lblMessage.ForeColor = Color.Blue;
                    lblMessage.Visible = true;
                    break;
                case State.Clear:
                    lblMessage.Text = String.Empty;
                    lblMessage.Visible = false;
                    break;

            }
        }

    }
}
