﻿namespace View
{
    partial class ManageCar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnExit = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.lblType = new System.Windows.Forms.Label();
            this.txtType = new System.Windows.Forms.TextBox();
            this.lblBrand = new System.Windows.Forms.Label();
            this.txtBrand = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblEngine = new System.Windows.Forms.Label();
            this.txtEngine = new System.Windows.Forms.TextBox();
            this.lblConstruction = new System.Windows.Forms.Label();
            this.txtConstruction = new System.Windows.Forms.TextBox();
            this.lblVehicleIdent = new System.Windows.Forms.Label();
            this.txtIdentification = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLicense = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(15, 357);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Size = new System.Drawing.Size(70, 25);
            this.lblMessage.TabIndex = 11;
            this.lblMessage.Text = "label1";
            this.lblMessage.Visible = false;
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(20, 416);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(106, 43);
            this.btnExit.TabIndex = 10;
            this.btnExit.Text = "Zurück";
            this.btnExit.UseVisualStyleBackColor = true;
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(272, 416);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(106, 43);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(14, 116);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(60, 25);
            this.lblType.TabIndex = 17;
            this.lblType.Text = "Type";
            // 
            // txtType
            // 
            this.txtType.Location = new System.Drawing.Point(237, 113);
            this.txtType.Name = "txtType";
            this.txtType.Size = new System.Drawing.Size(210, 31);
            this.txtType.TabIndex = 16;
            // 
            // lblBrand
            // 
            this.lblBrand.AutoSize = true;
            this.lblBrand.Location = new System.Drawing.Point(14, 69);
            this.lblBrand.Name = "lblBrand";
            this.lblBrand.Size = new System.Drawing.Size(69, 25);
            this.lblBrand.TabIndex = 15;
            this.lblBrand.Text = "Brand";
            // 
            // txtBrand
            // 
            this.txtBrand.Location = new System.Drawing.Point(236, 66);
            this.txtBrand.Name = "txtBrand";
            this.txtBrand.Size = new System.Drawing.Size(210, 31);
            this.txtBrand.TabIndex = 14;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(14, 20);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(68, 25);
            this.lblName.TabIndex = 13;
            this.lblName.Text = "Name";
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(237, 17);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(210, 31);
            this.txtName.TabIndex = 12;
            // 
            // lblEngine
            // 
            this.lblEngine.AutoSize = true;
            this.lblEngine.Location = new System.Drawing.Point(13, 164);
            this.lblEngine.Name = "lblEngine";
            this.lblEngine.Size = new System.Drawing.Size(209, 25);
            this.lblEngine.TabIndex = 19;
            this.lblEngine.Text = "EngineDisplacement";
            // 
            // txtEngine
            // 
            this.txtEngine.Location = new System.Drawing.Point(236, 161);
            this.txtEngine.Name = "txtEngine";
            this.txtEngine.Size = new System.Drawing.Size(210, 31);
            this.txtEngine.TabIndex = 18;
            // 
            // lblConstruction
            // 
            this.lblConstruction.AutoSize = true;
            this.lblConstruction.Location = new System.Drawing.Point(14, 208);
            this.lblConstruction.Name = "lblConstruction";
            this.lblConstruction.Size = new System.Drawing.Size(179, 25);
            this.lblConstruction.TabIndex = 21;
            this.lblConstruction.Text = "ConstructionYear";
            // 
            // txtConstruction
            // 
            this.txtConstruction.Location = new System.Drawing.Point(237, 205);
            this.txtConstruction.Name = "txtConstruction";
            this.txtConstruction.Size = new System.Drawing.Size(210, 31);
            this.txtConstruction.TabIndex = 20;
            // 
            // lblVehicleIdent
            // 
            this.lblVehicleIdent.AutoSize = true;
            this.lblVehicleIdent.Location = new System.Drawing.Point(14, 254);
            this.lblVehicleIdent.Name = "lblVehicleIdent";
            this.lblVehicleIdent.Size = new System.Drawing.Size(208, 25);
            this.lblVehicleIdent.TabIndex = 23;
            this.lblVehicleIdent.Text = "IdentificationNumber";
            // 
            // txtIdentification
            // 
            this.txtIdentification.Location = new System.Drawing.Point(237, 251);
            this.txtIdentification.Name = "txtIdentification";
            this.txtIdentification.Size = new System.Drawing.Size(210, 31);
            this.txtIdentification.TabIndex = 22;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 296);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(211, 25);
            this.label4.TabIndex = 25;
            this.label4.Text = "LicensePlateNumber";
            // 
            // txtLicense
            // 
            this.txtLicense.Location = new System.Drawing.Point(238, 293);
            this.txtLicense.Name = "txtLicense";
            this.txtLicense.Size = new System.Drawing.Size(210, 31);
            this.txtLicense.TabIndex = 24;
            // 
            // ManageCar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(486, 482);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtLicense);
            this.Controls.Add(this.lblVehicleIdent);
            this.Controls.Add(this.txtIdentification);
            this.Controls.Add(this.lblConstruction);
            this.Controls.Add(this.txtConstruction);
            this.Controls.Add(this.lblEngine);
            this.Controls.Add(this.txtEngine);
            this.Controls.Add(this.lblType);
            this.Controls.Add(this.txtType);
            this.Controls.Add(this.lblBrand);
            this.Controls.Add(this.txtBrand);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.lblMessage);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.btnSave);
            this.Name = "ManageCar";
            this.Text = "ManageCar";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Label lblType;
        private System.Windows.Forms.TextBox txtType;
        private System.Windows.Forms.Label lblBrand;
        private System.Windows.Forms.TextBox txtBrand;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblEngine;
        private System.Windows.Forms.TextBox txtEngine;
        private System.Windows.Forms.Label lblConstruction;
        private System.Windows.Forms.TextBox txtConstruction;
        private System.Windows.Forms.Label lblVehicleIdent;
        private System.Windows.Forms.TextBox txtIdentification;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtLicense;
    }
}