﻿using Controller;
using Model;
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using View.Common;

namespace View
{
    public partial class ManageOrder : Form
    {
        public ManageOrder()
        {
            InitializeComponent();
            ComboBoxSetter.Set(cmbBuyer);
            ComboBoxSetter.Set(cmbSeller);
            ComboBoxSetter.Set(cmbCar);
            ComboBoxSetter.Set(cmbOrderStatus);
            ComboBoxSetter.Set(cmbUser);
            DateTimePickerSetter.Set(dtpDate);
        }

        private void CreateOrder_Load(object sender, EventArgs e)
        {
            LoadInitalData();
        }

        /// <summary>
        /// Load initial UI data
        /// </summary>
        private void LoadInitalData()
        {
            try
            {
                LoadClients();
                LoadCars();
                LoadState();
                LoadUsers();
            }
            catch(Exception ex)
            {
                MessageLabelSetter.SetMessageLabel(lblMessage, MessageLabelSetter.State.Error, "Fehler beim laden der Daten");
            }
        }

        private void LoadUsers()
        {
            FillUserListBox(UserService.GetAllUser());
        }

        /// <summary>
        /// Fill combobox users with data and "Bitte wählen" entry
        /// </summary>
        /// <param name="list"></param>
        private void FillUserListBox(List<User> list)
        {
            list.Insert(0, new User(0, "Bitte wählen"));
            cmbUser.DataSource = list;

            cmbUser.DisplayMember = "Name";
            cmbUser.ValueMember = "Id";
        }


        private void LoadState()
        {
            /*cbStatus.DataSource = Enum.GetValues(typeof(Status));
Getting the enum from the selected item

Status status; 
Enum.TryParse<Status>(cbStatus.SelectedValue.ToString(), out status); */

            cmbOrderStatus.DataSource = Enum.GetValues(typeof(OrderStatus));
        }

        /// <summary>
        /// Loads Clients from Database and updates form
        /// </summary>
        /// <returns></returns>
        private void LoadClients()
        {
            try
            {
                FillClientListBox(cmbBuyer, ClientService.GetClients());
                FillClientListBox(cmbSeller, ClientService.GetClients());         
            }
            catch (Exception ex)
            {
                MessageLabelSetter.SetMessageLabel(lblMessage, MessageLabelSetter.State.Error, "Fehler beim laden der Kunden");
            }
        }

        /// <summary>
        /// Loads Cars from Database and updates form
        /// </summary>
        /// <returns></returns>
        private void LoadCars()
        {
            try
            {
                FillCarListBox(CarService.GetAllCars());
            }
            catch (Exception ex)
            {
                MessageLabelSetter.SetMessageLabel(lblMessage, MessageLabelSetter.State.Error, "Fehler beim laden der Autos");
            }
        }

        /// <summary>
        /// Fill combobox car with data and "Bitte wählen" entry
        /// </summary>
        /// <param name="list"></param>
        private void FillCarListBox(List<Car> list)
        {
            list.Insert(0, new Car(0, "Bitte wählen"));
            cmbCar.DataSource = list;

            cmbCar.DisplayMember = "Display";
            cmbCar.ValueMember = "Id";
        }

        /// <summary>
        /// Fill combobox buyer or seller with data and "Bitte wählen" entry
        /// </summary>
        /// <param name="cmb"></param>
        /// <param name="list"></param>
        private void FillClientListBox(ComboBox cmb, List<Client> list)
        {
            list.Insert(0, new Client(0, "Bitte wählen"));
            cmb.DataSource = list;

            cmb.DisplayMember = "Name";
            cmb.ValueMember = "ClientId";      
        }

        #region AddClientOrCar

        private void btnNewClient_Click(object sender, EventArgs e)
        {
            AddClient form = new AddClient();
            form.ShowDialog(this);
            LoadClients();
        }

        #endregion

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                MessageLabelSetter.SetMessageLabel(lblMessage, MessageLabelSetter.State.Clear);

                if ((int)cmbBuyer.SelectedValue > 0)
                {
                    if ((int)cmbCar.SelectedValue > 0)
                    {
                        if ((int)cmbOrderStatus.SelectedValue > 0)
                        {
                            if ((int)cmbSeller.SelectedValue > 0)
                            {
                                if ((int)cmbUser.SelectedValue > 0)
                                {

                                }
                                else
                                {
                                    MessageLabelSetter.SetMessageLabel(lblMessage, MessageLabelSetter.State.Info, "Erstellt von auswählen");
                                }
                            }
                            else
                            {
                                MessageLabelSetter.SetMessageLabel(lblMessage, MessageLabelSetter.State.Info, "Verkäufer auswählen");
                            }
                        }
                        else
                        {
                            MessageLabelSetter.SetMessageLabel(lblMessage, MessageLabelSetter.State.Info, "BestellStatus auswählen");
                        }
                    }
                    else
                    {
                        MessageLabelSetter.SetMessageLabel(lblMessage, MessageLabelSetter.State.Info, "Auto auswählen");
                    }
                }
                else
                {
                    MessageLabelSetter.SetMessageLabel(lblMessage, MessageLabelSetter.State.Info, "Käufer auswählen");
                }
            }
            catch(Exception ex)
            {
                MessageLabelSetter.SetMessageLabel(lblMessage, MessageLabelSetter.State.Error, "Fehler beim speichern der Order");
            }
        }

        private void btnManageCar_Click(object sender, EventArgs e)
        {
            ManageCar form = new ManageCar();
            form.ShowDialog(this);
            LoadCars();
        }
    }
}
