﻿using Model;
using Model.Repositorys;
using System;
using System.Collections.Generic;

namespace Controller
{
    public static class CarService
    {
        public static void CreateNewCar(string name, string brand, string type, 
            string engine, string construction, string identification, string license)
        {
            try
            {
                int engineInt = ValidateParamsStringToInt(engine);
                int constructionYear = ValidateParamsStringToInt(construction);

                CarRepository repository = new CarRepository();
                repository.Insert(new Car(name, brand, type, engineInt, constructionYear,
                    identification, license));

            }
            catch(FormatException fex)
            {
                throw fex;
            }
            catch(Exception ex)
            {
                throw new Exception("Error creating Car", ex);
            }
        }

        public static List<Car> GetAllCars()
        {
            try
            {
                CarRepository repository = new CarRepository();
                return repository.GetCars();
            }
            catch (Exception ex)
            {
                throw new Exception("Error reading cars from database", ex);
            }
        }

        /// <summary>
        /// Validates if input string can be parsed into an int
        /// </summary>
        /// <param name="param"></param>
        /// <returns>parsed int</returns>
        private static int ValidateParamsStringToInt(string param)
        {
            int x;

            if (Int32.TryParse(param, out x))
            {
                return x;
            }
            else
            {
                throw new FormatException("Parsing string to int failed");
            }
        }

    }
}
