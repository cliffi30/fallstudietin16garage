﻿using System;
using Model;
using Model.Repositorys;

namespace Controller
{
    class OrderService
    {
        Order curentOrder { get; set; }

        public void ChangeOrderStatus(int orderID, Enum statusNeu)
        {
            curentOrder = GetOrderByID(orderID);
            curentOrder.OrderStatus = OrderStatus.ORDERED;
        }

        private Order GetOrderByID(int orderID)
        {
            var ret = new Order();
            try
            {
                OrderRepository orp = new OrderRepository();
                ret = orp.GetOrderByID(orderID);
               
            }

            catch (Exception ex)
            {
                throw new Exception(String.Format("Problem beim Auslesen der Order aus der DB. ID = {0}", orderID));
            }
            return ret;

        }

    }
}
