﻿using Model;
using Model.Repositorys;
using System;
using System.Collections.Generic;

namespace Controller
{
    public static class ClientService
    {
        public static void IntertNewUser(string name, string address, string phoneNumber)
        {
            try
            {
                ClientRepository repository = new ClientRepository();
                repository.Insert(new Client(name, address, phoneNumber));
            }
            catch(Exception ex)
            {
                throw new Exception("Error insering new client");
            }
        }

        public static List<Client> GetClients()
        {
            try
            {
                ClientRepository repository = new ClientRepository();
                return repository.GetAll();
            }
            catch (Exception ex)
            {
                throw new Exception("Error getting clients", ex);
            }
        }

    }
}
