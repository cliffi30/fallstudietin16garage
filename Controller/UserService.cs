﻿namespace Controller
{
    public static class UserService
    {
        public static List<User> GetAllUser()
        {
            try
            {
                UserRepository repository = new UserRepository();
                return repository.GetUsers();
            }
            catch (Exception ex)
            {
                throw new Exception("Error reading cars from database", ex);
            }
        }
    }
}
        

